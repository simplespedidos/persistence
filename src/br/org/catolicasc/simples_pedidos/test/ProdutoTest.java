package br.org.catolicasc.simples_pedidos.test;

import org.junit.Test;

import br.org.catolicasc.simples_pedidos.dao.ProdutoDao;
import br.org.catolicasc.simples_pedidos.entity.Empresa;
import br.org.catolicasc.simples_pedidos.entity.Produto;
import br.org.catolicasc.simples_pedidos.entity.Usuario;
import br.org.catolicasc.simples_pedidos.persistence.JpaUtil;
import junit.framework.Assert;

public class ProdutoTest {
	private ProdutoDao produtoDao = new ProdutoDao();

	@Test
	public void inserTest() {
		Usuario u = new Usuario();
		u.setCpf("092313213234");
		u.setEmail("robert@gmail.com");
		u.setNome("robert");
		u.setSenha("132456");
		
		Empresa empresa = new Empresa();
		empresa.setCnpj("928108329");
		empresa.setEndereco("Rua dos loucos");
		empresa.setNomeFantasia("Doidos Malucos");
		empresa.setRazaoSocial("Hospicio LTDA");
		empresa.setTelefone("47213213123");
		empresa.setUsuario(u);
		
		Produto p = new Produto();
		p.setDescricao("Iphone");
		p.setPreco(1200D);
		p.setEmpresa(empresa);
		
		this.produtoDao.salva(p);
		Assert.assertFalse(this.produtoDao.listaTodos().isEmpty());
	}
}
