package br.org.catolicasc.simples_pedidos.test;

import org.junit.Test;

import br.org.catolicasc.simples_pedidos.dao.EmpresaDao;
import br.org.catolicasc.simples_pedidos.entity.Empresa;
import br.org.catolicasc.simples_pedidos.entity.Usuario;
import br.org.catolicasc.simples_pedidos.persistence.JpaUtil;
import junit.framework.Assert;

public class EmpresaTest {

	private EmpresaDao empresaDao = new EmpresaDao();

	@Test
	public void insertTest() {
		Usuario u = new Usuario();
		u.setCpf("09212332123");
		u.setEmail("duck@duck.com");
		u.setNome("Jorge");
		u.setSenha("123456");
		Empresa e = new Empresa();
		e.setCnpj("80981973981");
		e.setEndereco("Berta Weege, 200");
		e.setNomeFantasia("Fantasia");
		e.setRazaoSocial("Fantasia SA");
		e.setTelefone("3372-1160");
		e.setUsuario(u);
		empresaDao.salva(e);
		Assert.assertFalse(empresaDao.listaTodos().isEmpty());
	}

}
