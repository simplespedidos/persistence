package br.org.catolicasc.simples_pedidos.test;

import junit.framework.*;

import org.junit.Test;

import br.org.catolicasc.simples_pedidos.entity.Cliente;
import br.org.catolicasc.simples_pedidos.dao.ClienteDao;
import br.org.catolicasc.simples_pedidos.persistence.*;

public class ClienteTest {

	private ClienteDao clienteDao = new ClienteDao();

	@Test
	public void insertTest() {
		try {
			Cliente c = new Cliente();
			c.setCpf_cnpj("09245476936");
			c.setEmail("dap1995@gmail.com");
			c.setNome("Daniel");
			c.setCep("89251620");
			c.setEndereco("Rua dos loucos");
			clienteDao.salva(c);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		Assert.assertFalse(this.clienteDao.listaTodos().isEmpty());
	}
	
	@Test
	public void updateTest() {
		try {
			Cliente cliente = clienteDao.buscaPorld(1L);
			cliente.setCep("12345621");
			clienteDao.atualiza(cliente);
			Assert.assertEquals(clienteDao.buscaPorld(1L), cliente);
		} catch (Exception ex){
			System.out.println(ex.getMessage());
		}
	}

}
