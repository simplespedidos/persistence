package br.org.catolicasc.simples_pedidos.test;

import junit.framework.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.org.catolicasc.simples_pedidos.dao.EmpresaDao;
import br.org.catolicasc.simples_pedidos.dao.UsuarioDao;
import br.org.catolicasc.simples_pedidos.entity.Empresa;
import br.org.catolicasc.simples_pedidos.entity.Usuario;
import br.org.catolicasc.simples_pedidos.persistence.JpaUtil;

public class UsuarioTest {

	private UsuarioDao usuarioDao = new UsuarioDao();
	private EmpresaDao empresaDao = new EmpresaDao();

	@Test
	public void insertTest() {
		Usuario u = new Usuario();
		u.setCpf("08005166990");
		u.setEmail("luksfk@gmail.com");
		u.setNome("Matheus");
		u.setSenha("123");
		List<Empresa> empresas = new ArrayList<Empresa>();
		empresas.add(empresaDao.buscaPorld(1L));
		u.setEmpresas(empresas);
		this.usuarioDao.salva(u);
		Assert.assertFalse(this.usuarioDao.listaTodos().isEmpty());
	}

}
