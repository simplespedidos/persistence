package br.org.catolicasc.simples_pedidos.test;
import org.junit.Assert;
import org.junit.Test;
import br.org.catolicasc.simples_pedidos.dao.EstoqueDao;
import br.org.catolicasc.simples_pedidos.entity.Empresa;
import br.org.catolicasc.simples_pedidos.entity.Estoque;
import br.org.catolicasc.simples_pedidos.entity.ItemEstoque;
import br.org.catolicasc.simples_pedidos.entity.Produto;
import br.org.catolicasc.simples_pedidos.entity.Usuario;
import br.org.catolicasc.simples_pedidos.persistence.JpaUtil;

public class EstoqueTest {

	private EstoqueDao estoqueDao = new EstoqueDao();
	
	@Test
	public void insertTest() {
		
		Usuario u = new Usuario();
		u.setCpf("092313213234");
		u.setEmail("robert@gmail.com");
		u.setNome("robert");
		u.setSenha("132456");
		
		Empresa em = new Empresa();
		em.setCnpj("012932312");
		em.setEndereco("Rua lala");
		em.setNomeFantasia("Empresar");
		em.setRazaoSocial("EMPRESAR CIA LTDA");
		em.setUsuario(u);
		
		Produto produto = new Produto(em,"Android", 1000.0);
		Estoque e = new Estoque(em);
		ItemEstoque itemEstoque = new ItemEstoque(produto, e, 20000.0);
		e.adicionaProduto(itemEstoque);
		estoqueDao.salva(e);
		Assert.assertFalse(estoqueDao.listaTodos().isEmpty());

	}

}
