package br.org.catolicasc.simples_pedidos.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import br.org.catolicasc.simples_pedidos.dao.PedidoDao;
import br.org.catolicasc.simples_pedidos.entity.Cliente;
import br.org.catolicasc.simples_pedidos.entity.Empresa;
import br.org.catolicasc.simples_pedidos.entity.Estoque;
import br.org.catolicasc.simples_pedidos.entity.ItemEstoque;
import br.org.catolicasc.simples_pedidos.entity.ItemPedido;
import br.org.catolicasc.simples_pedidos.entity.Pedido;
import br.org.catolicasc.simples_pedidos.entity.Produto;
import br.org.catolicasc.simples_pedidos.entity.Usuario;
import br.org.catolicasc.simples_pedidos.persistence.JpaUtil;

public class Pedido_Item_PeditoTest {

	private PedidoDao pedidoDao = new PedidoDao();

	@Test
	public void insertTest() {
		
		Usuario u = new Usuario();
		u.setNome("Teste");
		u.setCpf("02132349232");
		u.setEmail("eleonora@empresa.com");
		u.setSenha("123456");
		
		Empresa e = new Empresa();
		e.setCnpj("80981973981");
		e.setEndereco("Berta Weege, 200");
		e.setNomeFantasia("Fantasia");
		e.setRazaoSocial("Fantasia SA");
		e.setTelefone("3372-1160");
		e.setUsuario(u);
		
		Cliente c = new Cliente();
		c.setCpf_cnpj("09245476936");
		c.setEmail("dap1995@gmail.com");
		c.setNome("Daniel");
		c.setCep("89251620");
		c.setEndereco("Rua dos loucos");
		Pedido pedido = new Pedido(c);
		
		
		Produto p = new Produto(e, "iphone", 1200.0);
		Estoque estoque = new Estoque(e);
		ItemEstoque itemEstoque = new ItemEstoque(p, estoque, 20000.0);
		estoque.adicionaProduto(itemEstoque);
		ItemPedido item1 = new ItemPedido(itemEstoque, pedido, 10.0);

		List<ItemPedido> itensPedido = new ArrayList<ItemPedido>();
		itensPedido.add(item1);

		pedido.setItens(itensPedido);

		Date data = new Date();
		pedido.setData(data);
		pedidoDao.salva(pedido);

	}

}
