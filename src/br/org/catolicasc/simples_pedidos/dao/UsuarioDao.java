package br.org.catolicasc.simples_pedidos.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.org.catolicasc.simples_pedidos.entity.Usuario;
import br.org.catolicasc.simples_pedidos.persistence.JpaUtil;

public class UsuarioDao extends JpaDaoBase<Usuario> {

	public Usuario existe(Usuario usuario) {
		EntityManager em = JpaUtil.getEntityManager();

		Query query = em.createQuery("from Usuario u where u.email = " + ":pLogin and u.senha = :pSenha");
		query.setParameter("pLogin", usuario.getEmail());
		query.setParameter("pSenha", usuario.getSenha());
		Usuario result = null;
		if (query.getResultList() != null && !query.getResultList().isEmpty()){
			result = (Usuario) query.getResultList().get(0);
		}
		return result;
	}

}
