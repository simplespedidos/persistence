package br.org.catolicasc.simples_pedidos.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Pedido implements Bean {

	@Id
	@GeneratedValue
	private Long id;

	private Date data;

	public Pedido() {
		super();
	}

	
	public Pedido(Cliente cliente){
		super();
		this.cliente = cliente;
	}
	
	@OneToMany(mappedBy = "pedido",cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private List<ItemPedido> itensPedido;

	public Pedido(Date data, Cliente cliente) {
		super();
		this.data = data;
		this.cliente = cliente;
	}

	@NotNull
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, fetch=FetchType.EAGER)
	private Cliente cliente;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;

	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public List<ItemPedido> getItens() {
		return this.itensPedido;
	}

	public void setItensPedido(List<ItemPedido> itensPedido) {
		this.itensPedido = itensPedido;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
/*
	public void adicionaItem(ItemEstoque itemEstoque, double quantidade) {
		itensPedido.add(new ItemPedido(itemEstoque, this, quantidade));
	}
*/
}
