package br.org.catolicasc.simples_pedidos.entity;

public interface Bean {

	Long getId();
	
	void setId(Long id);
}
