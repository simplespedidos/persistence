package br.org.catolicasc.simples_pedidos.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Produto implements Bean {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@Column(unique=true)
	private String descricao;

	@NotNull
	private Double preco;

	@NotNull
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, fetch=FetchType.EAGER)
	private Empresa empresa;
	
	public String getDescricao() {
		return descricao;
	}

	public Produto() {
		super();
	}

	public Produto(Empresa empresa, String descricao, Double preco) {
		super();
		this.empresa = empresa;
		this.descricao = descricao;
		this.preco = preco;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;

	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	

}
