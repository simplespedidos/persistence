package br.org.catolicasc.simples_pedidos.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Estoque implements Bean {

	@Id
	@GeneratedValue
	private Long id;

	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private List<ItemEstoque> itensEstoque;

	@NotNull
	@OneToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, fetch=FetchType.EAGER)
	private Empresa empresa;

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;

	}

	public List<ItemEstoque> getItensEstoque() {
		return itensEstoque;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void adicionaProduto(ItemEstoque itemEstoque) {
		this.itensEstoque.add(itemEstoque);
	}

	public Estoque() {
		super();
		this.itensEstoque = new ArrayList<ItemEstoque>();
	}	

	public Estoque(Empresa empresa) {
		super();
		this.itensEstoque = new ArrayList<ItemEstoque>();
		this.empresa = empresa;
	}	
}
