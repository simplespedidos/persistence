package br.org.catolicasc.simples_pedidos.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class ItemPedido implements Bean {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, fetch=FetchType.EAGER)
	private Pedido pedido;

	@NotNull
	@OneToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, fetch=FetchType.EAGER)
	private ItemEstoque itemEstoque;

	private double quantidade;

	private double preco;

	public double getPreco() {
		return preco;
	}

	private void setPreco(ItemEstoque itemEstoque) {
		this.preco = itemEstoque.getProduto().getPreco();
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	public boolean temProntaEntrega(){
		return this.quantidade <= itemEstoque.getQuantidade() ? true : false;
	}

	public ItemEstoque getProduto() {
		return itemEstoque;
	}

	public void setItemEstoque(ItemEstoque produto) {
		this.itemEstoque = produto;
	}

	public double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public double getPrecoTotal() {
		return this.preco * this.quantidade;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;

	}

	public ItemPedido(ItemEstoque produto, Pedido pedido, Double quantidade) {
		super();
		this.pedido = pedido;
		this.itemEstoque = produto;
		this.quantidade = quantidade;
		this.setPreco(produto);
	}
	
	public ItemPedido(){
		super();
	}

	public ItemEstoque getItemEstoque() {
		return itemEstoque;
	}

	public void setPreco() {
			this.preco = this.itemEstoque.getProduto().getPreco();
	}
	
}